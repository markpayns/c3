import bcrypt from 'bcryptjs';
const data = {
  users: [
    {
      name: 'Mark Paynaganan',
      email: 'markpayna@dudol.com',
      password: bcrypt.hashSync('123456'),
      isAdmin: true,
    },
    {
      name: 'John Doe',
      email: 'johndoe@gmail.com',
      password: bcrypt.hashSync('123456'),
      isAdmin: false,
    },
  ],
  products: [
    {
      name: 'Nike slim shirt',
      slug: 'nike-slim-shirt',
      category: 'Shirts',
      image: '/images/p1.jpg', // 679px x 829px
      price: 120,
      countInStock: 10,
      brand: 'Nike',
      rating: 5,
      numReviews: 10,
      description: 'high quality shirt',
    },
    {
      name: 'Adidas slim shirt',
      slug: 'adidas-slim-shirt',
      category: 'Shirts',
      image: '/images/p1.jpg',
      price: 120,
      countInStock: 10,
      brand: 'Adidas',
      rating: 4.5,
      numReviews: 10,
      description: 'high quality shirt',
    },
    {
      name: 'Nike slim pants',
      slug: 'nike-slim-pants',
      category: 'Pants',
      image: '/images/p1.jpg',
      price: 120,
      countInStock: 0,
      brand: 'Nike',
      rating: 3.5,
      numReviews: 10,
      description: 'high quality pants',
    },
    {
      name: 'Adidas slim pants',
      slug: 'adidas-slim-pants',
      category: 'Pants',
      image: '/images/p1.jpg',
      price: 120,
      countInStock: 10,
      brand: 'Nike',
      rating: 4,
      numReviews: 10,
      description: 'high quality pants',
    },
  ],
};
export default data;
